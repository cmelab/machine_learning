import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import pickle

def pmrand(n=1,a=100.):
    return (np.random.rand(n)-0.5)*a

def sample_eq(n=512,a=100.):
    eq = np.random.rand(n)*pmrand(a=a) + pmrand(a=a)
    eq -= np.mean(eq)
    eq /= np.std(eq)
    return eq

def sample_neq(n=512,a=100.):
    slope = -(np.random.rand()+.1)*a
    eq = np.arange(n)*slope/n+pmrand(n=n,a=a)
    eq -= np.mean(eq)
    eq /= np.std(eq)
    return eq

def sample_neq_exp(n=512,a=100.):
    eq = a*np.exp(-2*np.arange(n)/n)+pmrand(n=n,a=a) +pmrand(a=a)
    eq -= np.mean(eq)
    eq /= np.std(eq)
    return eq

def training_data(n_samples=10000): #first half equilibrated, second half not
    plots = sample_eq()
    labels = np.array([1.0,0.0])
    print("Generating  eq samples")
    for i in range(n_samples//2-1):
        plots = np.vstack((plots,sample_eq()))
        labels = np.vstack((labels,np.array([1.0,0.0]))) #first half equilibrated
    print("Generating  neq samples")
    for i in range(n_samples//2):
        func = sample_neq
        if i%2==0:
            func = sample_neq_exp
        plots = np.vstack((plots,func()))
        labels = np.vstack((labels,np.array([0.0,1.0])))
    return plots, labels

def simple(plots, labels,n=512,n_classes=2): #start with classes: equlibrated or not.
    #create model
    x = tf.placeholder(tf.float32,[None,n])
    W = tf.Variable(tf.zeros([n,n_classes]))
    b = tf.Variable(tf.zeros([n_classes]))
    y = tf.matmul(x,W)+b

    #define loss and optimizer
    y_ = tf.placeholder(tf.float32,[None,n_classes])
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    #start session
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    #train
    n = len(plots)
    h = n//2
    f = .8
    d = int(n*f/2)
    print("Starting to train")
    training_plots = plots[h-d:h+d]
    training_labels = labels[h-d:h+d]
    for _ in range(2000):
        sess.run(train_step, feed_dict={x:training_plots,y_:training_labels})

    #test trained model
    print("Starting to test")
    test_plots = np.vstack((plots[:h-d],plots[h+d:]))
    test_labels = np.vstack((labels[:h-d],labels[h+d:]))
    correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct,tf.float32))
    print("accuracy is: ",sess.run(accuracy,feed_dict={x:test_plots,y_:test_labels}))
    

if __name__=="__main__": 
    plots,labels = training_data()
    plots.dump("plots.dat")
    labels.dump("labels.dat")
    plots = np.load("plots.dat")
    labels = np.load("labels.dat")
    simple(plots,labels)
    #plt.plot(sample_eq())
    #plt.plot(sample_neq())
    #plt.plot(sample_neq_exp())
    #plt.show()

