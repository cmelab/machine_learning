import tensorflow as tf
#creating a symbolic variable placeholder x, None any number of images, 784 pixels in each image
x = tf.placeholder(tf.float32,[None,784])
#tensors full of zeros, going to teach values into variables
W = tf.Variable(tf.zeros([784,10]))
b = tf.Variable(tf.zeros([10]))
#multiplying x by W and then normalizing with softmax
y = tf.nn.softmax(tf.matmul(x,W)+b)
#defining cost how far off the model is from outcome we want
#y_ placeholder for correct values
y_ = tf.placeholder(tf.float32,[None,10])
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y),reduction_indices=[1]))
#shifting values at a rate of 0.5 in direction to reduce cost
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
#operation to initialize variables
init = tf.initialize_all_variables()
#run operation to intialize variables
sess = tf.Session()
sess.run(init)
#running training 1000 times
#batch of 100 random data points train_Step to feed batches into placerholder values
for i in range(1000):
    batch_xs, batch_yx = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
#how well does the model do
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
#returns boolean array of 0, 1
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
