from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data',one_hot=True)
#evaluating data as numpy arrays

#session to use c++
# to avoid building a computation graph manually?
import tensorflow as tf
sess = tf.InteractiveSession()
#placeholders
#any batch size, any amount of images
x = tf.placeholder(tf.float32,shape=[None,784])
y_ = tf.placeholder(tf.float32,shape=[None,10])
#weights and biases, tensors full of 0s
W = tf.Variable(tf.zeros([784,10]))
b = tf.Variable(tf.zeros([10]))
#initializing variables
sess.run(tf.global_variables_initializer())
#implementing model
y = tf.matmul(x,W) + b
#loss function, from cross entropy
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y,y_))
#training the model
#Gradient Descent Optimizer like calclulus?
#to update parameters
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
#training batch
#feed dict to replace placeholder values
for i in range(1000):
    batch = mnist.train.next_batch(100)
    train_step.run(feed_dict = {x: batch[0], y_: batch[1]})
#evaluating the model
correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print(accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
#same as original code
#improving model with weight initialization
#defined with activation potential algorith better with slightly positive inital bias
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)
#convolution and pooling boundaries
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides = [1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn. max_pool(x, ksize =[1, 2, 2, 1],
            strides=[1, 2, 2, 1], padding='SAME')
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])
#appling layer
x_image = tf.reshape(x, [-1, 28, 28, 1])
h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1)+b_conv1)
h_pool1 = max_pool_2x2(h_conv1)
#second layer
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2)+b_conv2)
h_pool2 = max_pool_2x2(h_conv2)
#reshape tensor from the pooling layer into a batch of vectors, multiply by weight
#matrix add a bias and apply a reLU (activation potential?)
W_fcl = weight_variable([7 * 7 * 64, 1024])
b_fcl = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fcl = tf.nn.relu(tf.matmul(h_pool2_flat, W_fcl)+b_fcl)
#dropout
