import os
import numpy as np
import random
import fnmatch
import cPickle as pickle
directory = 'eq_data/smaller_data/'
#directory = 'eq_data/generated_eq_data/'

class Eq_data():
#creates instance of class for each data set
#holds all information in class, eq or not ...

    def __init__(self,directory,filename):
        self.directory = directory
        self.filename = filename
        self.pe_data = self.eq_set()
        self.equilibrated = [] #maybe not used?
        self.eq_value = self.eq_or_not(self.filename)

    def eq_set(self):
        #imports data stores last 1000 lines in list
        #last 1000 as only the end matters for eq
        #read last nlines
        nlines = 1000
        data_set = []
        load_data= np.loadtxt(self.directory + self.filename, delimiter = '\n')
        data_set.append(load_data[nlines:])
        #to avoid bias algorithm all eq files then all non eq
        return data_set
    
    def eq_or_not(self,filename):
        #if eq_value is 0 not eq if 1 it is eq
        #determines if the file is eq or not based on filename 'eq','non_eq'
        if fnmatch.fnmatch(self.filename,'eq*'):
            print(filename)
            self.eq_value = np.array([1.0,0.0])
            return self.eq_value

        elif fnmatch.fnmatch(self.filename,'non_eq*'):
            print(filename)
            self.eq_value = np.array([0.0,1.0])
            return self.eq_value
    

def create_constructors(directory):
    constructors = []
    for filename in os.listdir(directory):
        constructors.append(Eq_data(directory,filename))
    total_constructs = len(constructors)
    return constructors,total_constructs

def pickle_data(constructors,filename):
    #pickle list of constructors for each file, with eq data
    pickle_out = open(str(filename)+".pickle","wb")
    pickle.dump(constructors,pickle_out)
    pickle_out.close()
    return

def load_pickle():
    pickle_in = open("smaller_data1.pickle","rb")
    example = pickle.load(pickle_in)
    print(example)
    return

if __name__=="__main__":
    constructors, total_constructs = create_constructors(directory)
    pickle_data(constructors,'smaller_data1')
    load_pickle()
