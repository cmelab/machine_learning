#https://pythonprogramming.net/train-test-tensorflow-deep-learning-tutorial/
#https://pythonprogramming.net/preprocessing-tensorflow-deep-learning-tutorial/

import tensorflow as tf
import cPickle as pickle
import numpy as np
from eq_learning import Eq_data
import random

def load_pickle(filename1):
    pickle_in = open(str(filename1)+".pickle","rb")
    data_construct = pickle.load(pickle_in)
    #print('type of data_construct is',type(data_construct))
    n = len(data_construct)
    tensor = np.array((n,1000))
    # need to add variable save?
    random.shuffle(data_construct)
    #print('THE TYPE OF TENSOR IS',type(tensor))
    #for i,d in enumerate(data_construct):
        #tensor[i]=np.array(d.pe_data)
    #print(data_construct[50].pe_data)
    #print(data_construct[50].eq_value)
    return tensor,data_construct

#train_x,train_y,test_x,test_y = data

# is 500 too much for now? hidden layers
n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500

#classes eq or not
n_classes = 2
batch_size = 50

#change later to last number of lines rather than 1000
#just string of values doesn't have to maintain shape
#if issues can get rid of None, 1000
#x data
#y label of data
x = tf.placeholder('float',[None,1000])
y = tf.placeholder('float')
y_ = tf.placeholder(tf.float32,[None,n_classes])

def network_model(data):
    #print('The type of data in network_model is',type(data))
    #train x is unessecary because of object oriented
    #need to randomize data
    #defining network based on sentdex video

    #bias added at the end, weights multiplied by input data
    #bias added to weight
    #(input data *weights) + bias to ensure non 0 even if all inputs are 0, last 1000 lines
    #variable is a random normal
    #basically weights in one tensor, will be modified, must have initial values so random
    #each node has a bias added so same shape
    #input would be number of node, hl1 -> hl2 ...
    #creating weight array of correct shape for this
    #output layer is n_classes because 1 hot set up [1,0] or [0,1]
    hidden1 = {'weights':tf.Variable(tf.random_normal([1000,n_nodes_hl1])),
                'biases':tf.Variable(tf.random_normal([n_nodes_hl1]))}
    #number of nodes is number in hl1 * hl2
    hidden2 = {'weights':tf.Variable(tf.random_normal([n_nodes_hl1,n_nodes_hl2])),
                  'biases':tf.Variable(tf.random_normal([n_nodes_hl2]))}
    hidden3 = {'weights':tf.Variable(tf.random_normal([n_nodes_hl2,n_nodes_hl3])),
                'biases':tf.Variable(tf.random_normal([n_nodes_hl3]))}
    output_layer = {'weights':tf.Variable(tf.random_normal([n_nodes_hl3,n_classes])),
                    'biases':tf.Variable(tf.random_normal([n_classes]))}
    
    #input_data * weights) + biases
    #should input be data, list of log files, no, need to access class
    #l1 = tf.add(tf.matmul(data,hidden1['weights']),hidden1['biases'])
    #works if using x, rather than data
    l1 = tf.add(tf.matmul(x,hidden1['weights']),hidden1['biases'])
    #threshold function applied to layer 1
    l1 = tf.nn.relu(l1)
    #l1 -> l2
    l2 = tf.add(tf.matmul(l1,hidden2['weights']),hidden2['biases'])
    l2 = tf.nn.relu(l2)
    
    l3 = tf.add(tf.matmul(l2,hidden3['weights']),hidden3['biases'])
    l3 = tf.nn.relu(l3)

    output = tf.matmul(l3,output_layer['weights']) + output_layer['biases']
    
    return output


def train_network(data):
    n1 = 512
    x = tf.placeholder(tf.float32,[None,n1])
    W = tf.Variable(tf.zeros([n1,n_classes]))
    b = tf.Variable(tf.zeros([n_classes]))
    y = tf.matmul(x,W)+b
    
    batch_data = []    
    batch_label = []
    #possible to pickle model, don't need to retrain every time
    #input data passing through network
    prediction = network_model(data)
    #calculates the difference between prediction and label, change y so actually label?
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    #minimizing cost, minimizing difference, using AdamOptimizer
    optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(cost)
    
    #optimizer = tf.train.AdamOptimizer().minimize(cost)

    #epoch "forward and back pass of all training examples, forward then fix weights"
    #"1000 examples, batch size epoch, 2 iterations, 1 epoch"
    n_epochs = 10
    #in tensorflow all things must be run in session
    #training data, optimizing weights
    with tf.Session() as session:
        session.run(tf.global_variables_initializer())
        #session.run(tf.initialize_all_variables()) old version of initalize function
        '''n = len(data)
        start = n//2
        training = 0.8
        end = int(n*training%/2)
        training_data = data'''

        for epoch in range(n_epochs):
            #calculate loss as you go?
            epoch_loss = 0
            #range in int(samples/batch size) = cycles
            #may want to run optimizer and cost and use feed_dict?
            i = 0
            print('i is ',i)
            print('length of data is ', len(data))
            while i < len(data):
                print('i is ',i)
                start = i
                end = i + batch_size
                batch_slice = np.array(data[start:end])
                print('batch_slice is ', len(batch_slice))
                for construct in range(len(data)):
                    batch_data += data[construct].pe_data
                    batch_label.append(data[construct].eq_value)
                #print('length of batch_label is ', len(batch_label))
                #print('batch_label ',batch_label, batch_label.shape)
                print('batch_data ',batch_data[0][0])
                #print('len 1',len(batch_data[1]), 'len2',len(batch_data[2]))

                session.run(optimizer,feed_dict={x:batch_data, y:batch_label})

                i+=batch_size
                batch_data = []
                batch_label = []
                #batch_y = []
        #argmax index of maximum value, both 1 hot so it will tell if identical
        #basically if prediction is [1,0] is it actually [1,0]
        #now actually running through model
        correct = tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
        #tf.cast changes variable type
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        #print('accuracy is: ', session.run(feed_dict={x:batch_data,y_:batch_label}))
        print('correct is ',correct)
        #accuracy images to labels
        #print('Accuracy',accuracy.eval())
        
    return

tensor, data = load_pickle('smaller_data1')
print('The type of data going into train_network is', type(data))
train_network(data)
#train_network(x) or train_network(data) should x be data variable, is x needed?
