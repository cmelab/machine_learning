import numpy as np
import math
import random

#generating noneq data of the form y = mx+b
def noneq_data():
    #generates list of time steps between 1 and a random integer by random step size
    time_step = list(np.arange(1,random.randint(100000,1000000),random.randint(10,100)))
    m = random.uniform(1,50)
    b = random.uniform(1,100)
    potential_e = []
    #c is to generate noise in the function
    for x in range(len(time_step)):
        c = random.uniform(1,5)
        energy = m*x+b+c
        potential_e.append(energy)
    return time_step, potential_e

#generates eq data flucating beweeen a range
def eq_data():
    time_step = list(np.arange(1,random.randint(100000,1000000),random.randint(10,100)))
    potential_e = []
    values = [random.uniform(1,100),random.uniform (1,100)]
    #to format data correctly for random.uniform (low,high)
    values.sort()
    for i in range(len(time_step)):
        potential_e.append(random.uniform(values[0],values[1]))
    return time_step, potential_e

def make_file(time_step,potential_e,eq_or_not,counter):
    data =''
    #formatting data into two columns
    for energy in potential_e:
        data = data + str(energy) + '\n'
    #writing file
    if eq_or_not == 'eq':
        filename = 'eq'
    else:
        filename = 'non_eq'
    directory = 'eq_data/smaller_data/' + filename + str(counter)
    savefile = open(directory, 'w')
    savefile.write(data)
    savefile.close()
    return

#generate files
non_eqfiles = 100
for i in range(non_eqfiles):
    time_step, potential_e = noneq_data()
    make_file(time_step,potential_e,'non',i)

eqfiles = 100
for i in range(eqfiles):
    time_step, potential_e = eq_data()
    make_file(time_step,potential_e,'eq',i)
