'''#read in images of time vs. potential energy graphs, determine from the shape whether the graph is in equilibrium or not

#from import imput_data

#eq = input_data.read_data_sets("EQ_data/",one_hot=True) 

#each image is q by q pixels, q2 total pixels

#eq.train.images shape [3,q2] starting with 3 plots to get program working
#will write python script later to autogenerate potential energy data
#either source from existing graphs, or generate numbers
#[1,0] not equilibrium
#[0,1] equilibrium
#eq.train.labels shape [3,2] two options available eq or not

#import tensorflow as tf
#symbolic variable
#x = tf.placeholder(tf.float32, [None,q2])
#weights and bias
w = tf.Variable(tf.zeros([q2,2]))
b = tf.Variable(tf.zeros([2]))
# implementing
y = tf.nn.softmax(tf.matmul(x,W)+b)
#placeholfer for correct answers
y_ = tf.placeholder(tf.float32,[None,2])
cross_entropy = tf.redice_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1])))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
init = tf.global_variables_initializer()
init = tf.initialize_all_variables()
#run operation to intialize variables
sess = tf.Session()
sess.run(init)
#running training 1000 times
#batch of 100 random data points train_Step to feed batches into placerholder values
for i in range(1000):
    batch_xs, batch_ys = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
#how well does the model do
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
#returns boolean array of 0, 1
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print(sess.run(accuracy, feed_dict={x: eq.test.images, y_: eq.test.labels}))

'''


